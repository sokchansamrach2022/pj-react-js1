import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {AuthProvider} from "./module/auth/compunents/Auth";
import {AuthInit} from "./module/auth/compunents/AuthInit";
import { setUpAxios } from './module/auth/compunents/AuthHelp';

const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios();
root.render(
    <React.StrictMode>
        <AuthProvider>
            <AuthInit>
                <App/>
            </AuthInit>
        </AuthProvider>
    </React.StrictMode>
);