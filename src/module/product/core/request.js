import axios from "axios";
//import {getAuth} from "../../auth/compunents/AuthHelp"

const getListProduct = (limit = 10, skip = 0) => {
    return axios.get("/products", {
        params: { limit, skip }
    });
}

const getListProductByCategory = (limit = 10, path = "") => {
    return axios.get(`/products/category/${path}`, {
        params: { limit }
    });
}

const getProductById = (id) => {
    return axios.get(`/products/${id}`);
}

const searchProduct = (params) => {
    return axios.get("/products/search", {
        params: params
    });
}

const getCategories = () => {
    return axios.get("/products/categories");
}

export {getListProduct, getProductById,searchProduct,getCategories,getListProductByCategory}