import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getListProduct, searchProduct ,getCategories,getListProductByCategory } from "../core/request"; // Assuming you have the searchProduct function
import "bootstrap/dist/css/bootstrap.min.css";
import Card from "react-bootstrap/Card";
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import PaginationBasic from "./Pagination";

const Listproducts = () => {
  const [products, setProducts] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [active, setActive] = useState("All");
  const [categories, setCategories] = useState([]);
  const [total, setTotal] = useState(0); // Define total state
  const navigate = useNavigate();

  const limit = 8;

  useEffect(() => {
      getListProduct(limit).then((response) => {
          setProducts(response.data.products);
          setTotal(response.data.total);
      });

      getCategories().then((response) => {
          setCategories(response.data);
      })
  }, []);

  useEffect(() => {
      active === "All" ?
          getListProduct(limit).then((response) => {
              setProducts(response.data.products);
              setTotal(response.data.total);
          })
          : getListProductByCategory(limit, active).then((response) => {
              setProducts(response.data.products);
              setTotal(response.data.total);

          });
  }, [active]);

  const onSearch = () => {
    searchProduct({ q: searchText }).then((response) => {
    setProducts(response.data.products);
    });
  };

  return (
    <div className="container mt-5">
      <div className="w-100 d-flex justify-content-center mb-4">
        <Form className="d-flex justify-content-center px-5 w-50">
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            onChange={(e) => setSearchText(e.target.value)}
          />
          <Button onClick={onSearch} variant="outline-success">
            Search
          </Button>
        </Form>
      </div>

      <div className="px-4">
        <div className="d-flex overflow-x-scroll w-100 pb-3 fs-2 text-uppercase">
          <Badge
            onClick={() => setActive("All")}
            className="px-4 py-2 fs-6 mx-2"
            bg={active === "All" ? "primary" : "secondary"}
          >
            All
          </Badge>
          {categories.map((item, index) => (
            <Badge
              onClick={() => setActive(item)}
              key={index}
              className="px-4 py-2 fs-6 mx-2 "
              bg={active === item ? "primary" : "secondary"}
            >
              {item}
            </Badge>
          ))}
        </div>
      </div>

      <div className="d-flex flex-wrap justify-content-center container text-start  ">
        {products.map((product, index) => (
          <div
            onClick={() => navigate(`product/${product.id}`)}
            className=" d-flex mx-2 mt-2 col mb-4"
            key={index}
          >
            <Card>
              <Card.Img
                variant="top"
                src={product.thumbnail}
                style={{ height: "200px", width: "300px" }}
              />
              <Card.Body>
                <Card.Title>
                  <p className=" text-center text-light bg-black">
                    Brand :{product.brand}
                  </p>
                </Card.Title>
                <Card.Text>
                  <p> Model: {product.title}</p>
                  <p>Discount: {product.discountPercentage}%</p>
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
      <div className="d-flex justify-content-center my-3">
        <PaginationBasic
          onChangePage={(page) => {
            getListProduct(limit, (page - 1) * limit).then((response) => {
              setProducts(response.data.products);
              setTotal(response.data.total);
            });
          }}
          total={total / limit}
        />
      </div>
    </div>
  );
};

export default Listproducts;
