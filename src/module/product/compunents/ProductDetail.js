import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getProductById } from "../core/request";
import "bootstrap/dist/css/bootstrap.min.css";
import Card from "react-bootstrap/Card";

const ProductDetail = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({});

  useEffect(() => {
    getProductById(id).then((response) => {
      setProduct(response.data);
    });
  }, [id]);

  console.log(id);

  if (!product.id) {
    return <div className="text-center fs-6">Loading...</div>;
  }

  return (
    <div className=" container justify-content-between pt-4 ">
      <div className="row">
        <div className="col-6">
          <Card.Img
            className=" img-fluid"
            variant="top"
            src={product.thumbnail}
            style={{ height: "400px", width: "100%" }}
          />
        </div>

        <div className="col-6 fs-5 fw-bold">
          <p>{product.title}</p>
          <p>{product.price} $</p>
          <p>
            Product Detail: <br />
            {product.description}
          </p>
          <p>Rating:{product.rating}  Deal: {product.discountPercentage}%</p>

        </div>
      </div>
    </div>
  );
};
export default ProductDetail;
