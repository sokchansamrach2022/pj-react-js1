
// This code defines a set of utility functions related to managing authentication tokens using the localStorage feature in web browsers.
import axios from "axios";

const setAuth =(vaule)=>localStorage.setItem("token",vaule)

const getAuth=()=> localStorage.getItem("token");

const removeAuth=()=>localStorage.removeItem("token");

const setUpAxios = () => {
    axios.defaults.baseURL = "https://dummyjson.com";
    axios.defaults.headers = {
      "Authorization": `Bearer ${getAuth()}`
    }
  }


export {setAuth,getAuth,removeAuth,setUpAxios}