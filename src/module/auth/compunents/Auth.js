import {useContext} from "react";
import {createContext, useState} from "react"
import * as authHelper from './AuthHelp'


const AuthContext = createContext({
    auth: null,
    saveAuth: function () {

    },
    logout: function () {

    },
    user: null,
    setUser: function () {

    }
});

const useAuth = () => useContext(AuthContext);

const AuthProvider = ({children}) => {

    const [auth, setAuth] = useState(authHelper.getAuth());
    const [user, setUser] = useState();


    const saveAuth = (auth) => {
        setAuth(auth)
        if (auth) {
            authHelper.setAuth(auth)
        } else {
            authHelper.removeAuth()
        }
    }

    const logout = () => {
        setAuth(null);
        authHelper.removeAuth();
    }

    return <AuthContext.Provider value={{
        auth,
        saveAuth,
        logout,
        user,
        setUser
    }}>
        {children}
    </AuthContext.Provider>


}

export {AuthProvider, useAuth}


