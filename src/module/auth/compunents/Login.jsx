import { useState } from "react";
import { useAuth } from "./Auth";
import { login } from "../core/request";

const Login = () => {
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [error, setError] = useState("");
  const { saveAuth } = useAuth();

  const onLogin = (event) => {
    event.preventDefault();
    login(username, pass)
      .then((response) => {
        setError("");
        saveAuth(response.data.token);
      })
      .catch((error) => {
        setError(error.response.data.message);
      });
  };

  return (
    <div className="container">
      <div className="row">
        `
        <div className="col-md-6 offset-md-3">
          <h2 className="text-center text-dark mt-5">Login Form</h2>
          <div className="text-center mb-5 text-dark">Made with bootstrap</div>
          <div className="card my-5">
            <form className="card-body cardbody-color p-lg-5">
              <div className="text-center">
                <img
                  src="https://cdn.pixabay.com/photo/2016/03/31/19/56/avatar-1295397__340.png"
                  className="img-fluid profile-image-pic img-thumbnail rounded-circle my-3"
                  width="200px"
                  alt="profile"
                />
              </div>

              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="Username"
                  aria-describedby="emailHelp"
                  placeholder="User Name"
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder="password"
                  onChange={(e) => setPass(e.target.value)}
                />
              </div>
              <div className="text-center   text-bg-light ">
                <button onClick={onLogin} className="btn btn-color px-2 mb-2">
                  Login
                </button>
              </div>
              <div className="text-center">{error}</div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
