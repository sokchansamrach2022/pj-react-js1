import axios from "axios";
import {getAuth} from "../compunents/AuthHelp";

const login = (username,pass) => {
   return  axios.post("https://dummyjson.com/auth/login", {
        username: username,
        password: pass,
    })
}
const getUser = () => {
  return   axios.get("https://dummyjson.com/auth/me", {
        headers: {
            "Authorization": `Bearer ${getAuth()}`
        }
    });
}
export {login,getUser}