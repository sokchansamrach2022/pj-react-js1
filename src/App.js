
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import HomePage from "./module/home";
import ErrorPage from "./module/error/ErrorPage";
import Layout from "./module/layout";
import "bootstrap/dist/css/bootstrap.min.css"
import Login from "./module/auth/compunents/Login";
import {useAuth} from "./module/auth/compunents/Auth";
import ProductDetail from "./module/product/compunents/ProductDetail";

const App = () => {


  const {auth} = useAuth();

  return (
    <BrowserRouter>
      <Routes>
        {auth ?
          <Route path="/" element={<Layout />}>
            <Route index element={<HomePage />} />
            <Route path="product/:id" element={<ProductDetail/>} />
            <Route path="*" element={<Navigate to="/" />}></Route>
          </Route>
          :
          <>
            <Route index path="auth/login" element={<Login />} />
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
        }
        <Route path="error" element={<ErrorPage />} />

      </Routes>
    </BrowserRouter>
  );
};


export default App;
